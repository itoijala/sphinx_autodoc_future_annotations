import setuptools

setuptools.setup(
    setup_requires=[
        'setuptools >= 40.4.3',
    ],
)
